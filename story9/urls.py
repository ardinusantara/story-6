from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home_story9, name='story9_page'),
    path('sign_up/', views.sign_up, name='sign_up'),
    path('sign_in/', views.sign_in, name='sign_in'),
    path('sign_out/', views.sign_out, name='sign_out'),
]
