from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from . import forms

def sign_up(request):
    user_form = forms.CreateUserForm()
    if request.method == 'POST':
        user_form = forms.CreateUserForm(request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.save()
            
            username = user_form.cleaned_data.get('username')
            messages.success(request, f'Successfully created account with username "{username}"')
            return redirect('/story9/sign_in/')
    content = {
        'form' : user_form,
    }
    return render(request, 'sign_up.html', content)

def sign_in(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return redirect('/story9/')
        else:
            messages.error(request, 'Your Username or Password is Incorrect!')

    return render(request, 'sign_in.html')

def sign_out(request):
    logout(request)
    return redirect('/story9/sign_in/')    

def home_story9(request):
    if request.user.is_authenticated:
        username = request.user.username
        content = {
            'username' : username
        }
        return render(request, 'home_story9.html', content)
    return render(request, 'home_story9.html')
