from django.test import TestCase, Client
from django.apps import apps
from .apps import Story9Config
from django.contrib.auth.models import User


# Create your tests here.
class TestApp(TestCase):
    def setUp(self):
        user = User.objects.create_user('gintoki', 'shiroyasha', 'shiroyasha')
        user.save()

    def test_app_is_exist(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')

    def test_sign_up_url_exists(self):
        response = self.client.get('/story9/sign_up/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'sign_up.html')
    
    def test_sign_up_from_page(self):
        user_count = User.objects.all().count()
        data = {
            'username':'sakatagintoki',
            'password1':'shiroyasha07',
            'password2':'shiroyasha07',
        }
        response = self.client.post('/story9/sign_up/', data = data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), user_count + 1)

    def test_sign_in_url_exists(self):
        response = self.client.get('/story9/sign_in/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'sign_in.html')
    
    def test_sign_in_success(self):
        data = {
            'username' : 'gintoki',
            'password' : 'shiroyasha',
        }
        response = self.client.post('/story9/sign_in/', data = data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 302)
        self.assertIn(content_response, 'Welcome, gintoki!')

    def test_sign_in_failed(self):
        data = {
            'username' : 'gintok',
            'password' : 'shiroyasha',
        }
        response = self.client.post('/story9/sign_in/', data = data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 200)
        self.assertIn('Your Username or Password is Incorrect!', content_response)
    
    def test_home_story9_url_exist(self):
        response = self.client.get('/story9/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'home_story9.html')

    def test_home_story9_signed_in_user(self):
        self.client.login(username='gintoki', password='shiroyasha')
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)
        content_response = response.content.decode('utf8')
        self.assertIn('Welcome, gintoki!', content_response)

    def test_logout_user(self):
        self.client.login(username='gintoki', password='shiroyasha')
        response = self.client.get('/story9/sign_out/')
        self.assertEquals(response.status_code, 302)