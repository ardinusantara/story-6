$(document).ready(function(){
    const allContents = $(".accordion_content").hide();

    $('.accordion_header').click(function(e) {
        e.preventDefault();
    
        var $this = $(this);
        
        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp();
        } else {
            $this.parent().parent().find('div .accordion_content').removeClass('show');
            $this.parent().parent().find('div .accordion_content').slideUp();
            $this.next().toggleClass('show');
            $this.next().slideToggle();
        }
    });

    $(".button_up, .button_down").click(function(e){
        e.stopPropagation(); 
        var $this = $(this);
        var accordItem = $this.parent().parent().parent();
        if($this.is(".button_up")) {
            accordItem.insertBefore(accordItem.prev());  
        } else {
            accordItem.insertAfter(accordItem.next());
        } 
    })
})