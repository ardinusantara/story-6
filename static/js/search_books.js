$(document).ready(function(){
    var input = document.getElementById("keyword");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("search_button").click();
        }
    });
    $("#search_button").click(function() {
        var isi_keyword = $("#keyword").val();
        if (isi_keyword) {
            var called_url = '/story8/data?q=' + isi_keyword;
        } else {
            var called_url = '/story8/data?q=naruto';
        }
        $.ajax({
            url: called_url,
            success : function(hasil) {
                console.log(hasil);
                var obj_hasil = $("#search_result");
                obj_hasil.empty();
                for (i = 0; i < hasil.items.length; i++) {
                    var title = hasil.items[i].volumeInfo.title;
                    if (hasil.items[i].volumeInfo.authors) {
                        var author = hasil.items[i].volumeInfo.authors;
                    } else {
                        var author = "The author for this book is not stated";
                    }
                    if (hasil.items[i].volumeInfo.categories) {
                        var category = hasil.items[i].volumeInfo.categories;
                    } else {
                        var category = "Category for this book is not available";
                    }
                    if (hasil.items[i].volumeInfo.description) {
                        var description = hasil.items[i].volumeInfo.description;
                    } else {
                        var description = "This book has no description"
                    }
                    if (hasil.items[i].volumeInfo.imageLinks) {
                        var thumbnails = hasil.items[i].volumeInfo.imageLinks.thumbnail;
                    } else {
                        var thumbnails = "https://media.tenor.com/images/e6be36bd6dc0c8428f8bf8bdcab8cef6/tenor.gif";
                    }
                    obj_hasil.append(
                        '<div class="content-section d-flex flex-row align-items-center"' + 
                        'style="border-radius: 1em">' + 
                        '<div class="mr-4"><img class="book_img" src=' + thumbnails + '></div>' + 
                        '<div><legend class="border-bottom mb-2"><strong>' + title + '</strong>' + 
                        '<small> (' + author + ')</small><br>' + '</legend>' + 
                        '<strong>Category : ' + category + '</strong><br>' +
                        '<p>' + description + '</p></div></div>');
                }
            }
        });
    });
    $("#search_button").click();
})