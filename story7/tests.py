from django.test import TestCase, Client
from django.apps import apps
from .apps import Story7Config

# Create your tests here.
class TestApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
    
    def test_is_story7_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

