from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def search_books(request):
    return render(request, "search_books.html")

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    response = requests.get(url)
    data = json.loads(response.content)
    return JsonResponse(data, safe=False)
