from django.test import TestCase, Client
from django.apps import apps
from .apps import Story8Config

# Create your tests here.
class TestApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

    def test_is_story8_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_is_url_data_exist(self):
        response = Client().get('/story8/data/?q=naruto')
        self.assertEqual(response.status_code, 200)
