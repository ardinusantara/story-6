from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.search_books, name='search_books'),
    path('data/', views.data, name='data'),
]
