from django.urls import path
from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.index, name='activity'),
    path('add/', views.add_activity, name='add_activity'),
    path('delete/<int:id>/', views.delete_activity, name='delete_activity'),
    path('delete-person/<int:id>/', views.delete_person, name='delete_person')
]
