from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from .models import Activity, Person

class MainTestCase(TestCase):
    def setUp(self):
        Activity.objects.create(name="Menanam Boba")
        Activity.objects.create(name="Memanen Boba")
        Person.objects.create(name="Pak Izuri", activity=Activity.objects.get(name="Menanam Boba"))
        Person.objects.create(name="Boba-kun", activity=Activity.objects.get(name="Memanen Boba"))
    def test_00_make_person(self):
        cnt = Person.objects.all().count()
        Person.objects.create(name='Bu Izuri', activity=Activity.objects.get(name="Menanam Boba"))
        self.assertEqual(Person.objects.all().count(), cnt+1)
    def test_01_make_activity_from_page(self):
        cnt = Activity.objects.all().count()
        response = self.client.post('/add/', data={'name':'Kegiatan'})
        self.assertEqual(Activity.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 302)
        
    def test_02_make_person_from_page(self):
        cnt = Person.objects.all().count()
        response = self.client.post('//', data={'name':'Dek Boba', 'activity' : '1'})
        self.assertEqual(Person.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 302)

    def test_03_delete_person_from_page(self):
        cnt = Person.objects.all().count()
        response = self.client.get(f'/delete-person/{Person.objects.all()[0].id}/')
        self.assertEqual(Person.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)

    def test_04_delete_activity_from_page(self):
        cnt = Activity.objects.all().count()
        response = self.client.get(f'/delete/{Activity.objects.all()[0].id}/')
        self.assertEqual(Activity.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)
