from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from .models import Activity, Person
from .forms import ActivityForm, PersonForm
from django.contrib import messages

context = {
    "activities" : "many activities",
    "form" : "this is form",
}

def index(request):
    context["activities"] = Activity.objects.all()
    form = PersonForm(request.POST or None)
    if(form.is_valid()):
        form.save()
        return redirect('story_6:activity')

    context["form"] = form
    return render(request, "main/activity.html", context)

def add_activity(request):

    form = ActivityForm(request.POST or None)
    if(form.is_valid()):
        form.save()
        return redirect('story_6:activity')

    context["form"] = form
    return render(request, "main/add_activity.html", context)


def delete_activity(request, id):
    activity = get_object_or_404(Activity, id=id)

    activity.delete()

    return redirect('story_6:activity')

def delete_person(request, id):
    person = get_object_or_404(Person, id=id)

    person.delete()

    return redirect('story_6:activity')
